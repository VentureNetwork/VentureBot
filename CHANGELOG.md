# Changelog

## 1.0.1

- Added GitLab bot to the Discord server.
- Migrated to GitLab due to security concerns.

## 1.0.0

- Forked reposity from [manuelgu/DiscordMC](https://github.com/manuelgu/DiscordMC)
