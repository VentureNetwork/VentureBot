# VentureBot-Bridge
VentureBot-Bridge is a plugin for [Spigot](https://spigotmc.org) which allows you to send messages from Minecraft to Discord and vice versa.

## Website
https://dynamicdonkey.github.io

## Dependencies
This plugins uses [Discord4j](https://github.com/austinv11/Discord4J) to interact with Discord

## Releases
Releases can be found under the [Releases] tab.

## Building
VentureBot-Bridge uses Maven to build
```
$ mvn clean install
``` 

## Contributing
You have an optimization or a new idea you want to implement? Don't hesitate to open a Pull Request against the `dev` branch
